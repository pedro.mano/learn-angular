import { Directive, ElementRef, Input, OnChanges, Renderer2 } from '@angular/core';

@Directive({
	selector: '[psmBorder]'
})
export class BorderDirective implements OnChanges {
	@Input() psmBorder = 1;
	constructor(private eleRef: ElementRef, private renderer: Renderer2) {

	}
	ngOnChanges(): void {
		this.renderer.setStyle(this.eleRef.nativeElement, "border", `${this.psmBorder}px solid black`);
	}
}
