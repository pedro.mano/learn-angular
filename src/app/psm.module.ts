import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './psm.component';
import { ListComponent } from './list/list.component';
import { PanelModule } from './panel/panel.module';
import { TabPanelComponent } from './tab-panel/tab-panel.component';
import { TabComponent } from './tab-panel/tab/tab.component';
import { BorderDirective } from './border.directive';
import { LowerCaseDirective } from './lower-case.directive';
import { SliderDirective } from './slider.directive';
import { FormsModule } from '@angular/forms';
import { RedCircleComponent } from './red-circle/red-circle.component';

@NgModule({
    declarations: [
        AppComponent,
        ListComponent,
        TabPanelComponent,
        TabComponent,
        BorderDirective,
        LowerCaseDirective,
        SliderDirective,
        RedCircleComponent
    ],
    providers: [],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        PanelModule,
        FormsModule
    ]
})
export class AppModule { }
