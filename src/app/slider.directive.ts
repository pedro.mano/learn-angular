import { Directive, ElementRef, EventEmitter, Input, OnChanges, Output } from '@angular/core';

declare let jQuery: any;

@Directive({
  selector: '[psmSlider]'
})
export class SliderDirective implements OnChanges {
  sliderRef: any;
  @Input() value?: number;
  @Output() valueChange = new EventEmitter();

  constructor(private eleRef: ElementRef) {
    this.sliderRef = jQuery(eleRef.nativeElement).slider({
      slide: (event: any, ui: any) => {
        this.valueChange.emit(ui.value);
      }
    });
  }

  ngOnChanges() {
    this.sliderRef.slider('option', { value: this.value });
  }
}
