import { Component, Directive, Input } from '@angular/core';

@Component({
  selector: 'psm-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent {
  @Input() title?: string;
}

@Directive({
  selector: 'psm-panel-header'
})
export class PanelHeaderDirective {
}