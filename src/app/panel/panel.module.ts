import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent, PanelHeaderDirective } from './panel.component';



@NgModule({
  declarations: [
    PanelComponent,
    PanelHeaderDirective
  ],
  exports: [
    PanelComponent,
    PanelHeaderDirective
  ],
  imports: [
    CommonModule
  ]
})
export class PanelModule { }
