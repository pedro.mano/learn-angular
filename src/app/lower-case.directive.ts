import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[psmLowerCase]'
})
export class LowerCaseDirective {
  @HostBinding() value = '';
  @HostListener('change', ['$event']) onChange($event: Event) {
    this.value = ($event.target as HTMLInputElement).value?.toLowerCase();
  }
}
