import { Component, Input } from '@angular/core';

@Component({
	selector: 'psm-tab',
	templateUrl: './tab.component.html',
	styleUrls: ['./tab.component.css']
})
export class TabComponent {
	@Input() title?: string;
	active = false;
}
