import { AfterContentInit, Component, ContentChildren, QueryList } from '@angular/core';
import { TabComponent } from './tab/tab.component';

@Component({
	selector: 'psm-tab-panel',
	templateUrl: './tab-panel.component.html',
	styleUrls: ['./tab-panel.component.css']
})
export class TabPanelComponent implements AfterContentInit {
	@ContentChildren(TabComponent) tabs!: QueryList<TabComponent>;

	ngAfterContentInit() {
		this.tabs.first.active = true;
	}

	activate(tab: TabComponent) {
		this.tabs.forEach((tab: TabComponent) => {
			tab.active = false;
		});
		tab.active = true;
	}
}
