import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

@Component({
	selector: 'psm-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ListComponent implements OnChanges {
	@Input() todoList?: string[];
	@Output() todoListChange = new EventEmitter<string[]>();

	delete(todo: string) {
		this.todoList = this.todoList?.filter((entry) => {
			return entry !== todo;
		});
		this.todoListChange.emit(this.todoList);
	}

	addTodo(todoEle: HTMLInputElement) {
		if (todoEle.value != "") {
			this.todoList?.push(todoEle.value);
			todoEle.value = "";
		}
	}

	getCurrentList(): string[] {
		return this.todoList ?? [];
	}

	ngOnChanges(): void {
		console.log("list", this.todoList);
	}
}
