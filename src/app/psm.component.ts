import { AfterViewInit, Component, OnChanges, ViewChild } from '@angular/core';
import { ListComponent } from './list/list.component';

@Component({
	selector: 'psm-root',
	templateUrl: './psm.component.html',
	styleUrls: ['./psm.component.css']
})
export class AppComponent implements OnChanges, AfterViewInit {
	@ViewChild('psmList') listCmps!: ListComponent
	title = 'learnC3';
	todoList: string[] = [];
	borderSize = 1;
	slideValue = 0;

	ngOnChanges() {
		console.log("component", this.todoList);
	}

	ngAfterViewInit() {
		console.log(this.listCmps.getCurrentList() == this.todoList);
	}

	isntAddable(): boolean {
		return this.todoList.length >= 4;
	}

	getListLength(): number {
		return this.todoList.length;
	}
}
